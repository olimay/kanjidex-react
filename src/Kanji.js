class Kanji {
  constructor(data) {
    this.id = data.kanji;
    this.grade = data.grade;
    this.stroke_count = data.stroke_count;
    this.meanings = data.meanings;
    this.kun_readings = data.kun_readings;
    this.on_readings = data.on_readings;
    this.name_readings = data.name_readings;
    this.jlpt = data.jlpt;
    this.unicode = data.unicode;
  }
}

export default Kanji;
