import React from 'react';
import './styles/KanjiCell.css';

const KanjiCell = ({ kanji, handleOnClick }) => {

  const ji = kanji;

  return (
    <button
      onClick={() => handleOnClick(ji)}
      className="kanji-cell btn-outline-dark btn-lg">
      <span className="kanji">{ji}</span>
    </button>);
};

export default KanjiCell;
