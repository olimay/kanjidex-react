import React from 'react';
import { jouyouKanji } from '../jouyouKanji';
import KanjiCell from './KanjiCell';
import './styles/KanjiList.css';

const  KanjiList = ({ handleOnClick }) => {
  const cells = jouyouKanji.map(kanji => {
    return (
      <KanjiCell
        key={kanji}
        kanji={kanji}
        handleOnClick={handleOnClick}
      />
    );
  });

  return (
    <section className="kanji-list overflow-auto">
      {cells}
    </section>
  )
}

export default KanjiList;
