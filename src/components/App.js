import React, { Component } from 'react';
import KanjiList from './KanjiList';
import DetailView from './DetailView';
import Kanji from '../Kanji';
import './styles/App.css';

const KANJIAPI = 'https://kanjiapi.dev/v1/kanji/';

class App extends Component {
  constructor() {
    super();
    this.state = {
      kanjiInfo: {}
    };

    this.handleOnClick = this.handleOnClick.bind(this);
  }

  loadKanjiInfo(kanji) {
  }

  handleOnClick(kanji) {
    var kanjiInfo;
    console.log(`${KANJIAPI+kanji}`);
    fetch(`${KANJIAPI+kanji}`)
    .then(response => response.json())
    .then(data => {
      const kanjiInfo = new Kanji(data);
      this.setState({ kanjiInfo });
      console.log(kanjiInfo);
    })
    .catch(err => console.log(err));
    return kanjiInfo;
  }

  render() {
    return (
      <div className="App container-fluid">
        <div className="row">
          <div className="col">
            <div className="jumbotron jumbotron-fluid">
                <h1 className="display-4">Kanjidex</h1>
                <p className="lead">Information on Japanese characters</p>
                <hr className="my-4" />
                <p>Data courtesy <a href="https://kanjiapi.dev/">https://kanjiapi.dev/</a>
                </p>
                <p>View the source code on <a href="https://gitlab.com/olimay/kanjidex-react">GitLab</a></p>
            </div>
          </div>
        </div>
        <div className="row">
          <div className="col-md-3 sticky-top">
            <DetailView kanjiInfo={this.state.kanjiInfo} />
          </div>
          <div className="col-md">
            <KanjiList
              handleOnClick={this.handleOnClick}
            />
          </div>
        </div>
      </div>
    );
  }
}

export default App;
