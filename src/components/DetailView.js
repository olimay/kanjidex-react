import React from 'react';
import './styles/DetailView.css';

const DetailView = ({ kanjiInfo }) => {
  const {
    id,
    grade,
    stroke_count,
    meanings,
    kun_readings,
    on_readings,
    name_readings,
    jlpt,
    unicode
  } = kanjiInfo;

  const showInfoList = (infoArray) => {
    let result = "";
    if (infoArray && infoArray.length > 0) {
      result = infoArray.join('; ');
    }
    return result;
  };

  return (
    <section className="Kanji-List sticky-top">
      <div className='data-wrapper'>
      <ul className='list-group'>
      <li className='kanji list-group-item display-2 text-center'>{id}</li>
      <li className="list-group-item">
        <strong>grade:  </strong>
        <span className='data-grade'>{grade}</span>
      </li>
      <li className="list-group-item">
        <strong>stroke-count:  </strong>
        <span className='data-stroke-count'>{stroke_count}</span>
      </li>
      <li className="list-group-item">
        <strong>meanings: </strong>
        <span className='data-meanings'>{showInfoList(meanings)}</span>
      </li>
      <li className="list-group-item">
        <strong>kun_readings: </strong>
        <span className='data-kun_readings'>{showInfoList(kun_readings)}</span>
      </li>
      <li className="list-group-item">
        <strong>on_readings: </strong>
        <span className='data-on_readings'>{showInfoList(on_readings)}</span>
      </li>
      <li className="list-group-item">
        <strong>name_readings: </strong>
        <span className='data-name_readings'>{showInfoList(name_readings)}</span>
      </li>
      <li className="list-group-item">
        <strong>jlpt: </strong>
        <span className='data-jlpt'>{jlpt}</span>
      </li>
      <li className="list-group-item">
        <strong>unicode: </strong>
        <span className='data-unicode'>{unicode}</span>
      </li>
      </ul>
      </div>
    </section>
  )
}

export default DetailView;
