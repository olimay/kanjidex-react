# kanjidex-react

View this project live: <https://olimay.gitlab.io/kanjidex-react/>

## About

Kanjidex lists the Jouyou Kanji set of Japanese characters. Selecting a
character will display information such as its meanings, readings, stroke count,
and Unicode escape code.

The data is provided by <https://kanjiapi.dev/>

### Colophon

This version uses [React](https://reactjs.org/) and
[Bootstrap](https://getbootstrap.com/).